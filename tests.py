import unittest as ut
from os import remove
from csvsplit import csvsplit


class csvTests(ut.TestCase):
    def setUp(self):
        pass
    
    def tearDown(self):
        remove("test_against_out_alice.csv")
        remove("test_against_out_bob.csv")
        remove("test_against_out_carol.csv")
        remove("test_against_out_eve.csv")
        pass
        
    def test_split(self):
        filenames = ["test_in_1.csv", "test_in_2.csv", "test_in_3.csv"]
        row_ids = ["alice", "bob", "carol", "eve"]
        out_name = "test_against_out_{row_id}.csv"
        # called as out_name.format(id=id)
        
        csvsplit(filenames=filenames, out_name=out_name)
        for row_id in row_ids:
            fn1 = out_name.format(row_id = row_id)
            fn2 = f"test_out_{row_id}.csv"
            
            with open(fn1) as f:
                f1 = f.read()
            
            with open(fn2) as f:
                f2 = f.read()
            
            self.assertEqual(f1, f2)
        

if __name__ == '__main__':
    ut.main()
