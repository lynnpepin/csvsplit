def csvsplit(filenames, out_name="out_{row_id}.csv", sep=","):
  row_ids = set() # list of string row_id
  #csvs = dict() # dict of string row_id --> list of string (representing the CSV to write)
  
  for filename in filenames:
    # For each file,
    # 1. Open the file and split into a list of line strings
    # 2. Collect a dict of string row_id --> list of int line_idx
    # 3. After going through each line, for each id, for line_idx in that dict, write to its own file.
    
    with open(filename) as f:
      lines = f.read().split("\n")
      row_id_to_line_idxs = dict()
      for line_idx, line in enumerate(lines):
        row_id = line.split(sep)[0]
        row_ids.add(row_id)
        
        if not row_id in row_id_to_line_idxs.keys():
            row_id_to_line_idxs[row_id] = list()
        
        row_id_to_line_idxs[row_id].append(line_idx)
      
      for row_id in row_id_to_line_idxs.keys():
        if not row_id in ("\n", ""):
          with open(out_name.format(row_id=row_id), "a+") as f:
            for line_idx in row_id_to_line_idxs[row_id]:
              f.write(lines[line_idx])
              f.write("\n")
  
  print(row_ids)
